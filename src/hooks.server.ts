import { handleHooks } from "@lucia-auth/sveltekit"
import type { Handle } from "@sveltejs/kit"
import { auth } from "$lib/server/lucia"

export const handle: Handle = async d => {

    d.event.setHeaders({
        "X-Frame-Options": "DENY"
    })

    return handleHooks(auth)(d)
}