import type { RequestHandler } from './$types'
import { error, json } from '@sveltejs/kit'

import { PrismaClient } from '@prisma/client'
import hashPassword from '../../../lib/server/hashPassword'
const prisma = new PrismaClient()

export const POST: RequestHandler = async ({ request }) => {
    const { username, password, phone, recovery } = await request.json()

    const user = await prisma.user.findFirst({
        where: {
            username: username.toLowerCase()
        }
    })

    // Check if user exists
    if (!user) {
        await prisma.log.create({
            data: {
                message: `Tried to reset phone number of a user that does not exist`,
                isCritical: true
            }
        })

        throw error(404, "A user with that username does not exist")
    }

    // If credentials are incorect, give 401
    if (user.password != await hashPassword(password, user.salt || "")) {

        await prisma.log.create({
            data: {
                message: `Failed reset phone number attempt for '${user.username}' (wrong credentials)`,
                isCritical: true
            }
        })

        throw error(401, "Credentials invalid")
    }

    // If recovery is incorrect, give 401
    if (user.recoveryToken?.replaceAll(" ", "") != recovery.replaceAll(" ", "")) {

        await prisma.log.create({
            data: {
                message: `Failed reset phone number attempt for '${user.username}' (wrong recoveryToken)`,
                isCritical: true
            }
        })

        throw error(401, "Recovery invalid")
    }

    // Create a temporary token in the database
    const token = await prisma.temporary_phone_token.create({
        data: {
            phone,
            token: Math.random().toString(36).substring(2, 8)
        }
    })

    // Send the token to the user via SMS
    await fetch('https://m183.gibz-informatik.ch/api/sms/message', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-Api-Key': process.env.SMS_API_TOKEN as string,
        },
        body: JSON.stringify({
            "mobileNumber": phone.replace("+", "").replaceAll(" ", ""), // TODO: Validate phone number
            "message": `Your confirmation code for changing your phone number at Blogging Duck is: ${token.token}`
        }),
    })

    await prisma.log.create({
        data: {
            message: `SMS Token for login sent to '${phone.replace("+", "").replaceAll(" ", "")}' (recovery)`
        }
    })
    
    // Return the token id to the user
    return json({
        tokenId: token.id
    })
}
