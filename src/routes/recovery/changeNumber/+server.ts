import type { RequestHandler } from './$types'

import hashPassword from '$lib/server/hashPassword'
import { auth } from '$lib/server/lucia'
import { error, json } from '@sveltejs/kit'

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export const POST: RequestHandler = async ({ request, locals }) => {
    const { username, phone, tokenId, token } = await request.json()

    if (!username?.length || !phone?.length || !token?.length)
        throw error(400)

    //#region Validate Token

    // Check if token and phone number match
    const dbToken = await prisma.temporary_phone_token.findFirst({
        where: {
            id: tokenId,
            phone
        }
    })
    if (token != dbToken?.token) {
        await prisma.log.create({
            data: {
                message: `Tried to reset phone number using invalid Token`,
                isCritical: true
            }
        })
        throw error(401, "Token invalid")
    }

    //#endregion

    const user = await prisma.user.findFirstOrThrow({
        where: {
            username: username.toLowerCase()
        }
    })

    await prisma.user.update({
        where: {
            id: user.id
        },
        data: {
            phone
        }
    })

    await prisma.log.create({
        data: {
            message: "Successfully change phone number of user",
            userId: user.id
        }
    })

    const session = await auth.createSession(user.id)
    locals.setSession(session)

    return new Response()
}