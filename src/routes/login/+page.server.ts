import type { PageServerLoad } from "./$types"
import { redirect } from "@sveltejs/kit"

// redirect if already logged in
export const load: PageServerLoad = async ({ locals }) => {
    const { user } = await locals.getSessionUser();
	console.log({user})
	if (user && !user.isAdmin) throw redirect(302, `/profile/${user.username}`)
	if (user && user.isAdmin) throw redirect(302, '/admin')
	return {}
};