import type { RequestHandler } from './$types'
import { error } from '@sveltejs/kit'
import { auth } from '$lib/server/lucia'
import hashPassword from '$lib/server/hashPassword'

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

const checkToken = async (phone: string, token: string) => {

    // Check if code matches
    const phoneToken = await prisma.temporary_phone_token.findFirst({
        where: {
            phone,
            token,
            createdAt: {
                // Check for 5 min validity
                gt: new Date(Date.now() - 300000)
            }
        }
    })

    return !!phoneToken
}

export const POST: RequestHandler = async ({ request, locals }) => {
    const { username, password, token } = await request.json()

    if (!username?.length || !password?.length)
        throw error(400, "Not all parameters are passed")

    const user = await prisma.user.findFirst({
        where: {
            username: username.toLowerCase()
        }
    })

    // Check if user exists
    if (!user) {
        await prisma.log.create({
            data: {
                message: `Tried to login to a user that does not exist`,
                isCritical: true
            }
        })

        throw error(404, "A user with that username does not exist")
    }

    // Delete failed login attempts older than 5 minutes
    await prisma.failedLoginAttempt.deleteMany({
        where: {
            occuredAt: {
                lt: new Date(Date.now() - 300000)
            }
        }
    })

    // Get count of remaining failed login attempts for this user
    const countOfFailedLoginAttempts = await prisma.failedLoginAttempt.count({
        where: {
            user
        }
    })

    if (countOfFailedLoginAttempts > 3) {
        await prisma.log.create({
            data: {
                message: `Failed login attempt limit reached`,
                isCritical: true,
                userId: user.id
            }
        })
        throw error(429, "More than three failed login attempts, please wait up to 5 minutes and try again")
    }

    // If credentials are incorect, increment FailedLoginAttempts, and give 401
    if (user.password != await hashPassword(password, user.salt || "")) {

        await prisma.log.create({
            data: {
                message: `Failed login attempt for '${user.username}' (wrong credentials)`,
                isCritical: true
            }
        })

        await prisma.failedLoginAttempt.create({
            data: {
                userId: user.id
            }
        })

        throw error(401, "Credentials invalid")
    }

    // TODO
    if (!user.phone) {
        await prisma.log.create({
            data: {
                message: `User without phone number tried to login (somehow?)`,
                isCritical: true
            }
        })

        throw error(400, "No phone number registered")
    }

    if (token?.length) {

        // Check if token is valid
        if (await checkToken(user.phone, token)) {

            // Delete all tokens related to the user (including this one)
            await prisma.temporary_phone_token.deleteMany({
                where: {
                    phone: user.phone
                }
            })

            // TODO: Maybe hash manually
            const session = await auth.createSession(user.id)
            locals.setSession(session)

            await prisma.log.create({
                data: {
                    message: "Successfully logged in using password",
                    userId: user.id
                }
            })

            return new Response(null, { status: 200 })

        } else {

            await prisma.log.create({
                data: {
                    message: `Failed login attempt for '${user.username}' (token)`,
                    isCritical: true
                }
            })

            await prisma.failedLoginAttempt.create({
                data: {
                    userId: user.id
                }
            })

            throw error(400, "Token invalid")
        }

    } else {

        // if no token -> send SMS
        const { token } = await prisma.temporary_phone_token.create({
            data: {
                phone: user.phone,
                token: Math.random().toString(16).substring(2, 8)
            }
        })

        await fetch('https://m183.gibz-informatik.ch/api/sms/message', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-Api-Key': process.env.SMS_API_TOKEN as string,
            },
            body: JSON.stringify({
                "mobileNumber": user.phone.replace("+", "").replaceAll(" ", ""),
                // TODO: Why no Emoji!?!??!
                "message": `Your confirmation code for registering at Blogging Duck is: ${token}`
            }),
        })

        await prisma.log.create({
            data: {
                message: `SMS Token for login sent to '${user.phone.replace("+", "").replaceAll(" ", "")}' (login)`
            }
        })

        return new Response("Token sent", { status: 201 })

    }
}