import type { PageServerLoad, Actions } from "./$types"
import { error, invalid } from "@sveltejs/kit"

import { PrismaClient } from "@prisma/client"
const prisma = new PrismaClient()

export const load: PageServerLoad = async ({ params, locals }) => {
    const { user } = await locals.getSessionUser()

    return {
        profileUser: await prisma.user.findFirst({
            where: {
                username: params.profileUsername
            },
            select: {
                username: true,
                name: true,
                emoji: true
            }
        }),
        posts: prisma.post.findMany({
            where: {
                OR: [
                    {
                        // Either status is public
                        status: "public"
                    }, {
                        // or I am the Author and the status is hidden
                        author: {
                            username: user?.username || ""
                        },
                        status: "hidden"
                    },
                ],
                author: {
                    username: params.profileUsername
                }
            },
            include: {
                _count: {
                    select: {
                        Comment: true
                    }
                }
            },
            orderBy: {
                publishedAt: "desc"
            }
        })
    }
}


export const actions: Actions = {
    changeEmoji: async ({ locals, request, params }) => {
        const session = await locals.getSession()
        if (!session)
            throw error(401)

        const user = await prisma.user.findFirstOrThrow({
            where: {
                username: params.profileUsername
            }
        })

        // make sure current user is the same user as the user that should be changed
        if (session.userId != user.id)
            throw error(403)

        const data = await request.formData()
        const emoji = data.get('emoji')

        if (!emoji?.toString().length)
            return invalid(400, { emoji, missing: true })

        // TODO: validate input

        await prisma.user.update({
            data: {
                emoji: emoji.toString()
            },
            where: {
                id: session.userId
            }
        })

        return { success: true }
    },
    changeName: async ({ locals, request, params }) => {
        const session = await locals.getSession()
        if (!session)
            throw error(401)

        const user = await prisma.user.findFirstOrThrow({
            where: {
                username: params.profileUsername
            }
        })

        // make sure current user is the same user as the user that should be changed
        if (session.userId != user.id)
            throw error(403)

        const data = await request.formData()
        const name = data.get('name')

        if (!name?.toString().length)
            return invalid(400, { name, missing: true })

        // TODO: validate input

        await prisma.user.update({
            data: {
                name: name.toString()
            },
            where: {
                id: session.userId
            }
        })

        return { success: true }
    }
}