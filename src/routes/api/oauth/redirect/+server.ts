import { auth } from "$lib/server/lucia"

import type { RequestHandler } from './$types'
import { error, json, redirect } from '@sveltejs/kit'

export const GET: RequestHandler = async ({ request, cookies }) => {
    const credentials = {
        clientId: "b24c3332a7ccd21ff9de"
    }

    const authUrl = new URL(`https://github.com/login/oauth/authorize`)

    // // the state can be stored in cookies or localstorage for request validation on callback
    // // cookies.delete("state")
    // cookies.set("state", state, {
    //     path: "/",
    //     httpOnly: true,
    //     maxAge: 60 * 60
    // })

    authUrl.searchParams.append("client_id", credentials.clientId)
    authUrl.searchParams.append("redirect_uri", "http://localhost:5173/api/oauth")
    authUrl.searchParams.append("state", "123asd")
    console.log(authUrl.toString())

    // redirect to authorization url
    throw redirect(307, authUrl.toString())
}