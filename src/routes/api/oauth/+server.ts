import { auth } from "$lib/server/lucia"

import type { RequestHandler } from './$types'
import { error, json, redirect } from '@sveltejs/kit'

import { PrismaClient } from "@prisma/client"
const prisma = new PrismaClient()

export const GET: RequestHandler = async ({ url, locals }) => {
    const credentials = {
        clientId: "b24c3332a7ccd21ff9de",
        clientSecret: "c88c8c8e1c11e721b017b04550d678ece036cfdc"
    }

    // get code and state from search params
    const code = url.searchParams.get("code") || ""
    const state = url.searchParams.get("state") || ""

    // // get state stored in cookie (refer to previous step)
    // const storedState = cookies.get("state")

    // // validate state
    // if (state !== storedState) throw error(500, "invalid state") // invalid state

    const authUrl = new URL("https://github.com/login/oauth/access_token")
    authUrl.searchParams.append("client_id", credentials.clientId)
    authUrl.searchParams.append("client_secret", credentials.clientSecret)
    authUrl.searchParams.append("code", code)
    authUrl.searchParams.append("redirect_url", "/")

    const response = await fetch(authUrl, {
        method: "POST"
    })

    const access_token = (await response.formData()).get("access_token")

    const userData = await fetch(`https://api.github.com/user`, {
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    })
    const { id, login } = await userData.json()

    let user = await prisma.user.findUnique({
        where: {
            githubId: id
        }
    })

    if (!user)
        user = await prisma.user.create({
            data: {
                githubId: id,
                name: login,
                username: login,
                phone: "" // TODO: Get rid
            }
        })

    locals.setSession(await auth.createSession(user.id))
    await prisma.log.create({
        data: {
            message: "Successfully logged in via GitHub",
            userId: user.id
        }
    })

    if (user.isAdmin)
        throw redirect(302, "/admin")
    else
        throw redirect(302, `/profile/${user.username}`)
}