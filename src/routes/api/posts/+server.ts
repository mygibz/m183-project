import type { RequestHandler } from './$types'
import { error, json } from '@sveltejs/kit'

import { PrismaClient } from "@prisma/client"
const prisma = new PrismaClient()

export const GET: RequestHandler = async ({ url, request }) => {
    const urlParameter = url.searchParams.get('token')
    const headerParameter = request.headers.get('token')

    if (!urlParameter && !headerParameter)
        throw error(401, "No token present")
    
    if (urlParameter && headerParameter && urlParameter != headerParameter)
        throw error(400, "Received two different tokens")

    const token = urlParameter || headerParameter as string

    const matchingTokenFound = await prisma.api_token.findUnique({
        where: {
            token
        }
    })

    if (!matchingTokenFound)
        throw error(401, "Invalid token")

    return json(
        await prisma.post.findMany({
            where: {
                status: {
                    in: ["public"]
                },
            },
            include: {
                author: {
                    select: {
                        name: true,
                        username: true,
                        emoji: true
                    }
                },
                Comment: true,
            }
        })
    )
}