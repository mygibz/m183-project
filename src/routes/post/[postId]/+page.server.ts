import type { PageServerLoad, Actions } from "./$types"
import { error, invalid, redirect } from "@sveltejs/kit"

import { PrismaClient } from "@prisma/client"
const prisma = new PrismaClient()

export const actions: Actions = {
    // changeStatus: async ({ locals, params }) => {
    //     const { user } = await locals.getSessionUser()
    //     if (!user)
    //         throw error(401)

    //     const post = await prisma.post.findUniqueOrThrow({
    //         where: {
    //             id: params.postId
    //         }
    //     })

    //     if (user.userId != post.userId)
    //         throw error(403)

    //     if (post.status == "deleted")
    //         throw error(404, "Post has been deleted")

    //     await prisma.post.update({
    //         where: {
    //             id: post.id
    //         },
    //         data: {
    //             status: post.status == "public" ? "hidden" : "public"
    //         }
    //     })
    // },
    delete: async ({ locals, params }) => {
        const { user } = await locals.getSessionUser()
        if (!user)
            throw error(401)

        const post = await prisma.post.findUniqueOrThrow({
            where: {
                id: params.postId
            }
        })

        if (user.userId != post.userId) {
            await prisma.log.create({
                data: {
                    message: `Attempted to delete post of another user`,
                    isCritical: true,
                    userId: user.userId
                }
            })

            throw error(403)
        }

        await prisma.post.update({
            where: {
                id: post.id
            },
            data: {
                status: "deleted"
            }
        })

        throw redirect(307, "/")
    },
    comment: async ({ locals, request, params }) => {
        const session = await locals.getSession()
        if (!session)
            throw error(401)

        const data = await request.formData()
        const message = data.get('message')

        if (!message?.toString().length)
            return invalid(400, { message, missing: true })

        // Max length of 200
        if (message?.toString().length > 200)
            return invalid(400, { message, invalid: true })

        const { id } = await prisma.comment.create({
            data: {
                message: message.toString(),
                userId: session.userId,
                postId: params.postId
            }
        })

        await prisma.log.create({
            data: {
                message: `Posted a post '${id}', replying to '${params.postId}'`,
                userId: session.userId
            }
        })

        return { success: true }
    }
}

export const load: PageServerLoad = async ({ params, locals }) => {
    const { user } = await locals.getSessionUser()

    let filters: any[] | undefined = undefined
    if (!user?.isAdmin)
        filters = [
            {
                // either status is public
                status: "public"
            }, {
                // or I am the Author and the status is hidden
                author: {
                    username: user?.username || ""
                },
                status: "hidden"
            }
        ]

    return {
        post: await prisma.post.findFirst({
            where: {
                OR: filters,
                id: params.postId
            },
            include: {
                author: {
                    select: {
                        username: true,
                        name: true,
                        emoji: true
                    }
                },
                Comment: {
                    orderBy: [
                        {
                            commentedAt: "desc"
                        }
                    ],
                    include: {
                        commentor: {
                            select: {
                                name: true,
                                username: true,
                                emoji: true
                            }
                        }
                    }
                }
            }
        })
    }
}