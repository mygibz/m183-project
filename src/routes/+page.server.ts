import type { PageServerLoad, Actions } from "./$types"
import { error, invalid } from "@sveltejs/kit"

import { PrismaClient } from "@prisma/client"
const prisma = new PrismaClient()

export const load: PageServerLoad = async () => {
    return {
        posts: await prisma.post.findMany({
            where: {
                status: {
                    in: ["public"]
                },
            },
            include: {
                author: {
                    select: {
                        username: true,
                        name: true,
                        emoji: true
                    }
                },
                _count: {
                    select: {
                        Comment: true
                    }
                }
            },
            orderBy: {
                publishedAt: "desc"
            }
        })
    }
}

export const actions: Actions = {
    post: async ({ locals, request }) => {
        const session = await locals.getSession()
        if (!session)
            throw error(401)

        const data = await request.formData()
        const message = data.get('message')

        if (!message?.toString().length)
            return invalid(400, { message, missing: true })

        // TODO: validate input

        console.log({ message })

        const { id } = await prisma.post.create({
            data: {
                content: message.toString(),
                status: "hidden",
                author: {
                    // FYI, I've never done this before and I LOVE that prisma can do this so easily (it could even create one of it didn't exist!!!)
                    connect: {
                        id: session.userId
                    }
                }
            }
        })

        await prisma.log.create({
            data: {
                message: `Posted a post: '${id}'`,
                userId: session.userId
            }
        })

        return { success: true }
    }
}