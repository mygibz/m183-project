import type { PageServerLoad } from "./$types"
import { error } from "@sveltejs/kit"

import { PrismaClient } from "@prisma/client"
const prisma = new PrismaClient()

export const load: PageServerLoad = async ({ locals }) => {
  // Check if the user is an admin before making a request to the database
  const { user } = await locals.getSessionUser();
  if (!user?.isAdmin) throw error(403, "Forbidden, you are not an admin")

  // Get the logs from the database
  const logs = await prisma.log.findMany({
    orderBy: {
      loggedAt: "desc"
    },
    include: {
      user: {
        select: {
          emoji: true,
          name: true,
          username: true
        }
      }
    }
  })

  return { logs }
}