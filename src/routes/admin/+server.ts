import type { RequestHandler } from './$types'
import { error, json } from '@sveltejs/kit'
import totpGenerator from 'totp-generator'

import { PrismaClient } from "@prisma/client"
const prisma = new PrismaClient()

export const POST: RequestHandler = async ({ request, locals }) => {
    const { user } = await locals.getSessionUser();
    if (!user?.isAdmin)
        throw error(403, "Forbidden, you are not an admin")

    const { postId, newStatus, totp } = await request.json()

    if (!postId.length || !newStatus.length)
        throw error(400, "Body not sent or invalid")

    if (totpGenerator(process.env.TOTP_TOKEN || "") != totp)
        throw error(403, "TOTP Token invalid")

    await prisma.post.update({
        where: {
            id: postId
        },
        data: {
            status: newStatus
        }
    })

    await prisma.log.create({
        data: {
            message: `Changed the status of '${postId}' to '${newStatus}'`,
            userId: user.userId
        }
    })
   
    return json(null)
}