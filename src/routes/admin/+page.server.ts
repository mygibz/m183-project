import type { PageServerLoad } from "./$types"
import { error } from "@sveltejs/kit"

import { PrismaClient } from "@prisma/client"
const prisma = new PrismaClient()

export const load: PageServerLoad = async ({ locals }) => {
    const { user } = await locals.getSessionUser();

    if (!user?.isAdmin)
        throw error(403, "Forbidden, you are not an admin")

    return {
        posts: await prisma.post.findMany({
            include: {
                author: {
                    select: {
                        username: true,
                        name: true,
                        emoji: true
                    }
                },
                _count: {
                    select: {
                        Comment: true
                    }
                }
            },
            orderBy: {
                publishedAt: "desc"
            }
        })
    }
}