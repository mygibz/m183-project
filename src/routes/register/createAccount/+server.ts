import type { RequestHandler } from './$types'

import hashPassword from '$lib/server/hashPassword'
import { auth } from '$lib/server/lucia'
import { error, json } from '@sveltejs/kit'

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export const POST: RequestHandler = async ({ request, locals }) => {
    const { username, password, phone, tokenId, token } = await request.json()

    if (!username?.length || !password?.length || !phone?.length || !token?.length)
        throw error(400)

    // TODO: Check if user name already exists


    //#region Validate Token

    // Check if token and phone number match
    const dbToken = await prisma.temporary_phone_token.findFirst({
        where: {
            id: tokenId,
            phone
        }
    })
    if (token != dbToken?.token) {
        await prisma.log.create({
            data: {
                message: `Tried to register using invalid Token`,
                isCritical: true
            }
        })
        throw error(401, "Token invalid")
    }

    //#endregion

    const salt = Math.random().toString(16).substring(2, 16)
    const hashed_password = await hashPassword(password, salt)

    const user = await prisma.user.create({
        data: {
            name: username,
            username: username.toLowerCase(),
            password: hashed_password,
            salt,
            phone,
            recoveryToken: `${Math.random().toString(16).substring(2, 8)} ${Math.random().toString(16).substring(2, 8)} ${Math.random().toString(16).substring(2, 8)} ${Math.random().toString(16).substring(2, 8)}`
        }
    })
    const session = await auth.createSession(user.id)
    locals.setSession(session)

    await prisma.log.create({
        data: {
            message: "Successfully registered",
            userId: user.id
        }
    })

    return json({
        "recoveryToken": user.recoveryToken
    })
}