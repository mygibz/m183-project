import type { RequestHandler } from './$types'
import { json } from '@sveltejs/kit'

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export const POST: RequestHandler = async ({ request }) => {
    const { phone } = await request.json()

    // Create a temporary token in the database
    const token = await prisma.temporary_phone_token.create({
        data: {
            phone,
            token: Math.random().toString(36).substring(2, 8)
        }
    })

    // Send the token to the user via SMS
    await fetch('https://m183.gibz-informatik.ch/api/sms/message', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-Api-Key': process.env.SMS_API_TOKEN as string,
        },
        body: JSON.stringify({
            "mobileNumber": phone.replace("+", "").replaceAll(" ", ""), // TODO: Validate phone number
            "message": `Your confirmation code for registering at Blogging Duck is: ${token.token}`
        }),
    })

    await prisma.log.create({
        data: {
            message: `SMS Token for login sent to '${phone.replace("+", "").replaceAll(" ", "")}' (registration)`
        }
    })
    
    // Return the token id to the user
    return json({
        tokenId: token.id
    })
}
