export const validatePassword = (password: string) => {
    if (password.length < 8)
        return "Password must be at least 8 characters long."

    if (!/[a-z]/.test(password))
        return "Password must contain at least one lowercase letter."

    if (!/[A-Z]/.test(password))
        return "Password must contain at least one uppercase letter."

    if (!/\d/.test(password))
        return "Password must contain at least one digit."

    if (!/[^a-zA-Z0-9]/.test(password))
        return "Password must contain at least one special character."

    return ""
}

const validateUsername = (username: string) => {
    if (username.length < 8)
        return "Username must be at least 8 characters long"

    if (username.includes(" "))
        return "Username must not contain spaces"

    if (!username.match(/^[a-zA-Z0-9]+$/))
        return "Username can only contain letters and numbers"

    return ""
}