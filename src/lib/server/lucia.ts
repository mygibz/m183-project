import lucia from "lucia-auth";
import prisma from "@lucia-auth/adapter-prisma"

import { PrismaClient } from "@prisma/client"
const client = new PrismaClient()

export const auth = lucia({
	adapter: prisma(client),
    env: "DEV",
    
	transformUserData: (userData) => {
		return {
			userId: userData.id,
            name: userData.name,
            username: userData.username,
            isAdmin: userData.isAdmin,
		};
	}
})

export type Auth = typeof auth