import argon2 from 'argon2'

export default async (password: string, salt: string) => (await argon2.hash(password, {
    salt: Buffer.from(salt),
    raw: true
})).toString("hex")