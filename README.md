# Blogging 🦆

Eine Blog-Application die für das M183 Modul am GIBZ erstellt wurde.

## Beispiel Nutzerdaten

Dies sind Nutzer, welche in der Datenbank vorhanden sind, und zum Login verwendet werden können.

- Rolle: Nutzer<br>
  Nutzername: SomeUser<br>
  Passwort: 3MFiGpx39wZ8ekaE<br>
  Recovery: 79f3ce 2e4009 fd040f 82e83a

- Rolle: Administrator<br>
  Nutzername: God<br>
  Passwort: G9xvRUsb3fwn9fQu
  Recovery: 56e6b3 50c626 f5c333 3dd47f

Ebenfalls existiert ein API-Endpoint auf dem /api/posts-Pfad. Dieser benötigt ein Token (Entweder als URL Parameter "token" oder als Header "token") aus der Datenbank in der api_token-Tabelle. Dies ist ein Beispiel eines solchen Tokens (der zur Testzwecken verwendet werden kann): 5A4qn72pENvi2kZNJGAsb7DqBVsi9hbZooeEf3vqdFncficJXAat7WSwQd4id4Q3

## Verwendete Bibliotheken

### Basics

- svelte: Hauptpaket von Svelte, das die Svelte Sprache bereitstellt.
- vite: Ist ein Build-Tool/Bundler für moderne Web-Anwendungen, welches sehr effizient ist.
- @sveltejs/kit: Stellt Server-Side-Rendering (SSR) und Routing bereit.
- @sveltejs/adapter-auto: Adapter für SvelteKit, der es ermöglicht, verschiedene Build-Tools und -Workflows zu verwenden.

### Auth

- lucia-auth: Dies ist ein Paket, das Authentifizierungsfunktionen wie Benutzerregistrierung, -anmeldung und -verwaltung bereitstellt.
- totp-generator: Dieses Paket generiert und überprüft Time-Based One-Time Passwords (TOTP), wird hier für die Statusänderung eines Posts durch den Administrator verwendet.
- argon2: Dieses Paket hasht und überprüft Passwörter mithilfe des Argon2-Passwort-Hashing-Algorithmus. Argon2 ist für seine hohe Widerstandsfähigkeit gegen Brute-Force-Angriffe und andere Arten von Passwort-Cracking-Techniken bekannt.

### Datenbank

- prisma: Ein ORM, inklusive Migrations- und Deployment-Werkzeuge.
- @prisma/client: Dies ist der offizielle Prisma-Client, der es ermöglicht, auf die Datenbank (durch das obrige Paket verwaltet) zuzugreifen und sie zu manipulieren.

## Setup (.env)

Bevor die Applikation gestartet werden kann, muss eine .env-Datei im Root-Verzeichnis des Projektes erstellt werden (alternativ auch als Environment-Variabel bereitgestellt). Für die Demo hier ist dies aber bereits ausgefüllt

- SMS_API_TOKEN: Diese Environment-Variable beinhaltet den Token, der das Backend verwendet, um sich beim SMS-Server zu Authentifizieren.
- TOTP_TOKEN: Ein String im Base32 Format, der als TOTP Key verwendet werden soll (generieren mit z.B. `LC_ALL=C tr -dc 'A-Z2-7' </dev/urandom | head -c 32; echo`). Der Admin soll diesen Token in sein Passwortmanager speichern im TOTP Codes zu generieren (alternativ kann auch [totp.app](https://totp.app/) verwendet werden)

Zum starten der Applikation muss Node und Npm installiert sein, danach können einfach diese Befehle ausgeführt werden im Lokal zu entwickeln: `npm install` und `npm run dev`.

Wichtig anzumerken ist, dass die oauth-credentials direkt in den Code eingebettet sind, da nicht davon ausgegangen wird, dass der Tester (Herr Gisler) diese selbst erstellen will.

## Verfahren zur Speicherung von Passwörtern

Um die Passwörter sicher zu speichern, wird ein Hashing-Algorithmus verwendet. In diesem Fall Argon2. Bei der Registrierung (ausgenommen oauth) wird ein zufälliges Salz generiert (mit `Math.random().toString(16).substring(2, 16)`). Dieses Salz wird in der Datenbank gespeichert. Das Passwort wird danach mittels dem Argon2-Algorithmus, zusammen mit dem Salz, gehascht und der Hash wird dann in der Datenbank gespeichert.

Beim Login wird das Salz und gehaschte Passwort dann aus der Datenbank ausgelesen. Das vom Nutzer eingegebene Passwort wird mit dem Salz aus der Datenbank zusammen-gehascht und mit dem in der Datenbank vorhandenen Hash verglichen.

Dies ist Sicher, da das Passwort nie im Klartext in der Datenbank gespeichert wird und der Hashing-Algorithmus nur in eine Richtung funktioniert. Da das Salz bei jedem Nutzer anders ist, können auch keine Rainbow-Tables verwendet werden. Somit bleibt einem Angreifer (unter der Annahme, dass er die Datenbank irgendwie erbeuten konnte) nichts weiter übrig als Brute-Force zu verwenden, was sehr sehr lange dauern würde.

### SMS Tokens

Jeder Nutzer (exklusive via oauth) hat beim Registrierungsprocess eine Telefonnummer zu hinterlegen, welche mittels eines SMS und eines temporären Tokens zugang erlaubt. Hierfür wird ein für dieses Modul bereitgestellten SMS-Server verwendet.

Unter *Login -> I lost access to my phone number*, kann die Telefonnummer mittels der Recovery-Daten zurückgesetzt werden.

## Schutz vor Attacken

Das verwendete Framework (SvelteKit) beinhaltet selbst einen Schutz vor Cross-Site-Scripting (XSS) Attacken.

Schutz vor Clickjacking Attacken wurde mithilfe des *X-Frame-Options*-Headers, in der *hooks.server.ts*-Datei bereitgestellt.

## Logging

Logs werden in einer eigenen Tabelle in der Datenbank erstellt. Es wird zwischen kritischen (Fehlgeschlagene Anfragen und Login Versuche, ...) und nicht kritischen Logs (Post oder Kommentar created, erfolgreiche Login/Registration, Token versendet, ...) unterschieden.

Diese Unterscheidung wurde getroffen, um eventuelle Sicherheitsprobleme (wie z.B. eine Attacke) zu erkennen und zu loggen, als auch um als Logbuch zur Aufzeichnung der Aktivitäten zu dienen.